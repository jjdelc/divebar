#!/usr/bin/env python

from setuptools import setup

requires = [
    'pyramid', 'gunicorn', 'python-twitter==3.5', 'boto', 'beautifulsoup4',
    'pelican', 'markdown', 'unicode-slugify', 'hashfs', 'mf2py', 'rq'
    ]
test_requires = ['pytest', 'mock']

entry_points = {
    'console_scripts': [
        'divebar = divebar.cli:main',
        'start_divebar_api = divebar.api.run:run_server',
        'start_queue = divebar.queue:run_worker',
        'send_notifications = divebar.api.run:send_notifications',
    ]
}

README = open('README.md').read()


setup(
    name='divebar',
    version='0.0.1',
    url='http://www.isgeek.net/',
    author='Jj Del Carpio',
    author_email='jj@isgeek.net',
    description="Static indieweb blog",
    long_description=README,
    packages=['divebar'],
    include_package_data=True,
    install_requires=requires,
    extras_require={
        'testing': test_requires
    },
    dependency_links=[
        'https://github.com/erikriver/opengraph/tarball/master'
    ],
    entry_points=entry_points,
    classifiers=[
        'Development Status :: 1 - Planning',
        'Environment :: Web Environment',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Topic :: Internet :: WWW/HTTP',
    ],
    test_suite='divebar.tests',
)
