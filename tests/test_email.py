# coding: utf-8

import tempfile
from email.mime.text import MIMEText
from textwrap import dedent

import mock

from divebar.api.email import process_notifications, send_notifications, \
    prepare_body
from divebar.db import Notifications
from divebar.pelecanus import settings


NOTIFICATIONS = [
    {
        "url": "/path/1/",
        "payload": {
            "author": {"name": "FriendUser", "url": "https://twitter.com/FriendUser"},
            "is_like": True,
        },
    },
    {
        "url": "/path/1/",
        "payload": {
            "author": {"name": "FriendUser2", "url": "https://twitter.com/FriendUser2"},
            "is_rt": True,
        },
    },
    {
        "url": "/path/2/",
        "payload": {
            "author": {
                "name": "FriendUser3",
                "url": "https://instagram.com/FriendUser3",
            },
            "is_rt": True,
        },
    },
]


def test_process_notifications():
    expected = dedent(
        """
    /path/1/ (2 reactions)
        Like from FriendUser - https://twitter.com/FriendUser
        Rt from FriendUser2 - https://twitter.com/FriendUser2
        
    /path/2/ (1 reactions)
        Rt from FriendUser3 - https://instagram.com/FriendUser3
    """
    ).lstrip()
    body = process_notifications(NOTIFICATIONS)
    assert body == expected


def test_email():
    e_settings = {
        "HOST": "smtp.example.com",
        "PORT": 666,
        "FROM": "test@example.com",
        "PASS": "secretsecret",
        "TO": "target@example.com",
    }

    with tempfile.NamedTemporaryFile() as fh:
        db = Notifications(fh.name)
        for fixture in NOTIFICATIONS:
            db.add_notification("verb", fixture["url"], fixture["payload"])

        ctx = prepare_body(db)
        msg = MIMEText(ctx["body"])
        msg["Subject"] = ctx["subject"]
        msg["From"] = e_settings["FROM"]
        msg["To"] = e_settings["TO"]

        settings.update({"DB": fh.name})
        with mock.patch("divebar.api.email.SMTP_SSL") as smtp:
            send_notifications(e_settings)

    assert smtp.mock_calls[0] == mock.call(e_settings["HOST"], e_settings["PORT"])
    assert smtp.mock_calls[1] == mock.call().login(
        e_settings["FROM"], e_settings["PASS"]
    )
    assert smtp.mock_calls[2] == mock.call().sendmail(
        e_settings["FROM"], [e_settings["TO"]], msg.as_bytes()
    )
    assert smtp.mock_calls[3] == mock.call().quit()
