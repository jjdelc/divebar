# coding: utf-8

from divebar.api.readlinks import parse_contents, is_youtube_video


def test_parse_no_title():
    # This HTML document does not have a <title> tag
    r = parse_contents("""<html><head></head><body><body></html>""", None)
    assert r == {"_url": None, "scrape": False, "summary": None}


def test_parse_title():
    r = parse_contents("""<html><head><title>Yes</title></head><body><body></html>""", None)
    assert r == {"_url": None, "scrape": False, "summary": None, "title": "Yes"}


def test_parse_youtube_playlist():
    url = """https://www.youtube.com/playlist?list=PLar4u0v66vIodqt3KSZPsYyuULD5meoAo"""
    assert not is_youtube_video("", url)


def test_parse_youtube_search():
    url = """https://www.youtube.com/results?search_query=%23MegaFavNumbers"""
    assert not is_youtube_video("", url)
