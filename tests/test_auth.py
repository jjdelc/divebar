from divebar.api.auth import get_access_token, verify_access_token


def test_access_tokens():
    settings = {
        'ACCESS_TOKEN_SECRET': 'qwerty'
    }
    for x in range(10):
        settings['SALT_LENGTH'] = x
        scope = str(x)
        token = get_access_token(settings, scope)
        assert verify_access_token(settings, token, scope)
        # Sending a different scope fails
        assert not verify_access_token(settings, token, scope + '!')

