import tempfile
from urllib.parse import urljoin
from unittest import TestCase

from pyramid.exceptions import HTTPBadRequest

from divebar.api.webmention import validate_mention
from divebar.pelecanus import settings, HARD_SETTINGS


OUTPUT_PATH = tempfile.TemporaryDirectory()
MEDIA_UPLOADS = tempfile.TemporaryDirectory()
MEDIA_RECENT_FILES = tempfile.TemporaryFile()


TEST_SETTINGS = {
    'SITEURL': 'http://blog.localhost',
    'API_HOST': 'http://localhost',
    'ACCESS_TOKEN_SECRET': 'qwerty',
    'MEDIA_RECENT_FILES': MEDIA_RECENT_FILES.name,
    'MEDIA_UPLOADS': MEDIA_UPLOADS.name,
    'SALT_LENGTH': 5,
    'OUTPUT_PATH': OUTPUT_PATH.name,
    'MEDIA_URL': '/media/'
}
TEST_SETTINGS.update(HARD_SETTINGS)
settings.update(TEST_SETTINGS)  # So all modules have the same settings


class TestValidateMention(TestCase):
    def test_ok(self):
        source = "http://source.com"
        target = urljoin(settings["SITEURL"], "/post/")
        self.assertIsNone(validate_mention(source, target))

    def test_bad_rarget(self):
        source = "http://source.com"
        target = "https://not.this.blog/post"
        with self.assertRaises(HTTPBadRequest) as err:
            validate_mention(source, target)
        self.assertEqual(err.exception.status_code, 400)


class TestFindMention(TestCase):
    pass

