import tempfile
from unittest import TestCase

from divebar.db import Notifications, PicturesDB


class TestNotificationsDB(TestCase):
    def test_insert(self):
        with tempfile.NamedTemporaryFile() as fh:
            db = Notifications(fh.name)
            db.add_notification("like", "https://example.com/path/like", {
                "source": "X"
            })
            db.add_notification("webmention", "https://example.com/path/webmebtion", {
                "source": "Y"
            })
            db.add_notification("share", "https://example.com/path/rt", {
                "source": "Z"
            })
            self.assertEqual(3, db.count())
            nots = list(db.all_notifications())
            self.assertCountEqual(
                [r["payload"] for r in nots],
                [{"source": "X"}, {"source": "Y"}, {"source": "Z"}]
            )

            self.assertCountEqual(
                {r["verb"]: r["url"] for r in nots},
                {
                    "like": "https://example.com/path/like",
                    "webmention": "https://example.com/path/webmention",
                    "share": "https://example.com/path/rt"
                }
            )
            db.clear()
            self.assertEqual(0, db.count())


class TestPicturesDB(TestCase):
    def test_read_recent(self):
        with tempfile.NamedTemporaryFile() as fh:
            db = PicturesDB(fh.name)
            db.add_picture("/path/pic1.jpg")
            db.add_picture("/path/pic2.jpg")
            recent = db.recent()
            self.assertEqual([x["url"] for x in recent], ["/path/pic2.jpg", "/path/pic1.jpg"])
            db.add_picture("/path/pic3.jpg")

            recent = db.recent()
            self.assertEqual([x["url"] for x in recent],
                ["/path/pic3.jpg", "/path/pic2.jpg", "/path/pic1.jpg"])

    def test_recent_limit(self):
        url_mask = "/path/pic{}.jpg"
        with tempfile.NamedTemporaryFile() as fh:
            db = PicturesDB(fh.name)
            expected = []
            for x in range(20):
                url = url_mask.format(x)
                expected.append(url)
                db.add_picture(url)

            recent = db.recent()
            self.assertEqual([x["url"] for x in recent], list(reversed(expected))[:PicturesDB.RECENT_LIMIT])
            recent = db.recent(5)
            self.assertEqual([x["url"] for x in recent], list(reversed(expected))[:5])