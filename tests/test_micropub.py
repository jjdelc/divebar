import json
import tempfile
from datetime import datetime
from io import BytesIO
from unittest import TestCase
from urllib.parse import urljoin

import mock
from pyramid import testing
from pyramid.httpexceptions import HTTPMethodNotAllowed, HTTPBadRequest
from webob.multidict import MultiDict

from divebar import core
from divebar.api import micropub
from divebar.api.auth import get_access_token
from divebar.api.micropub import MicropubEndpoint, OPTS_CORS_RESP
from divebar.pelecanus import settings, HARD_SETTINGS


OUTPUT_PATH = tempfile.TemporaryDirectory()
MEDIA_UPLOADS = tempfile.TemporaryDirectory()
MEDIA_RECENT_FILES = tempfile.TemporaryFile()

TEST_SETTINGS = {
    'SITEURL': 'http://blog.localhost',
    'API_HOST': 'http://localhost',
    'ACCESS_TOKEN_SECRET': 'qwerty',
    'MEDIA_RECENT_FILES': MEDIA_RECENT_FILES.name,
    'MEDIA_UPLOADS': MEDIA_UPLOADS.name,
    'SALT_LENGTH': 5,
    'OUTPUT_PATH': OUTPUT_PATH.name,
    'MEDIA_URL': '/media/'
}
TEST_SETTINGS.update(HARD_SETTINGS)
settings.update(TEST_SETTINGS)  # So all modules have the same settings


class MockCGIFieldStorage:
    def __init__(self, file, filename):
        self.file = file
        self.filename = filename


def auth_request(params=None, path='/', post=None):
    return testing.DummyRequest(params=params, headers={
        'Authorization': 'Bearer {}'.format(get_access_token(settings, ''))
    }, post=MultiDict(post) if post else None, path=path)


class TestMicroPubEndpoint(TestCase):
    def test_allowed_methods(self):
        request = auth_request()
        request.method = 'PUT'
        with self.assertRaises(HTTPMethodNotAllowed):
            MicropubEndpoint(request)()
        request.method = 'PATCH'
        with self.assertRaises(HTTPMethodNotAllowed):
            MicropubEndpoint(request)()

    def test_options_no_auth(self):
        request = testing.DummyRequest()
        request.method = 'OPTIONS'
        response = MicropubEndpoint(request)()
        self.assertEqual(response, OPTS_CORS_RESP)


class TestMicropubConfig(TestCase):
    def test_no_param(self):
        request = auth_request(params={})
        with self.assertRaises(HTTPBadRequest):
            MicropubEndpoint(request)()

    def test_config(self):
        request = auth_request({
            'q': 'config'
        })
        resp = MicropubEndpoint(request)()
        mp_config = {
            "syndicate-to": [],
            "media-endpoint": urljoin(settings['API_HOST'], "media")
        }
        self.assertEqual(json.loads(resp.body.decode('utf-8')), mp_config)

    def test_syndication(self):
        request = auth_request({
            'q': 'syndicate-to'
        })
        retval = [{'uid': 'soocial'}]
        with mock.patch('divebar.api.micropub.get_syndication') as gs:
            gs.return_value = retval
            resp = MicropubEndpoint(request)()
        self.assertTrue(gs.called)
        self.assertEqual(json.loads(resp.body.decode('utf-8')), {
            'syndicate-to': retval
        })


class TestSourcePost(TestCase):
    def test_bad_source(self):
        request = auth_request({
            'q': 'source'
            # Not including a URL
        })
        with self.assertRaises(HTTPBadRequest):
            MicropubEndpoint(request)()

        request = auth_request({
            'q': 'source',
            'url': 'not a blog post'
        })
        with self.assertRaises(HTTPBadRequest):
            MicropubEndpoint(request)()

    def test_source_post(self):
        filename = 'test_source_post'
        post_url = '/filename/'  # It is mocked, it doesn't matter
        request = auth_request({
            'q': 'source',
            'url': post_url
        })
        post_time = datetime.now()
        post_meta = [
            ('title', 'post title'),
            ('content', 'post content'),
            ('date', post_time)
        ]
        with mock.patch('divebar.api.micropub.source_file_for_url') as sffu:
            sffu.return_value = filename
            with mock.patch('divebar.api.micropub.read_post_meta') as rpm:
                rpm.return_value = post_meta
                resp = MicropubEndpoint(request)()
        sffu.assert_called_with(post_url)
        rpm.assert_called_with(filename, include_content=True)
        meta_dict = dict(post_meta)
        self.assertEqual(json.loads(resp.body.decode('utf-8')), {
            'type': ['h-entry'],
            'properties': {
                'published': [meta_dict['date'].isoformat()],
                'name': [meta_dict['title']],
                'content': [meta_dict['content']]
            }
        })


class TestNewPost(TestCase):
    mp_func = staticmethod(lambda m: 'divebar.api.micropub.{}'.format(m))

    def test_bad_date_format(self):
        request = auth_request(post={
            'h': 'entry',
            'published': 'unparsable date'
        })
        with self.assertRaises(HTTPBadRequest) as err:
            MicropubEndpoint(request)()
        self.assertEqual(err.exception.message, "Invalid publish date")

    def test_vanilla_post_with_title(self):
        post_date = datetime.now()
        content = 'content of blog post'
        post_name = 'my blog post'
        request = auth_request(post={
            'h': 'entry',
            'name': post_name,
            'content': content,
            'published': post_date.isoformat()
        })
        with mock.patch(self.mp_func('generate_new_post_file')) as gnpf, \
                mock.patch(self.mp_func('post_file')), \
                mock.patch(self.mp_func('process_file')), \
                mock.patch(self.mp_func('send_webmentions')) as swm:
            response = MicropubEndpoint(request)()
        title, slug, ht = core.make_title(post_name, post_date)
        url = core.build_url(post_date, slug)
        url = urljoin(settings['SITEURL'], url)
        gnpf.assert_called_with(title, slug, content, [
            ('display_format', 'entry'),
            ('link', url)
        ], post_date)
        swm.assert_called_with(url)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.headers['Location'], url)

    def test_vanilla_post_without_title(self):
        content = 'content of blog post'
        request = auth_request(post={
            'h': 'entry',
            'content': content,
        })
        post_date = datetime.now()
        with mock.patch(self.mp_func('datetime')) as mock_datetime:
            mock_datetime.now.return_value = post_date
            mock_datetime.side_effect = lambda *a, **kw: datetime(*a, **kw)
            with mock.patch(self.mp_func('generate_new_post_file')) as gnpf, \
                    mock.patch(self.mp_func('post_file')), \
                    mock.patch(self.mp_func('process_file')), \
                    mock.patch(self.mp_func('send_webmentions')) as swm:
                response = MicropubEndpoint(request)()
        title, slug, ht = core.make_title('', post_date)
        url = core.build_url(post_date, slug)
        url = urljoin(settings['SITEURL'], url)
        gnpf.assert_called_with(title, slug, content, [
            ('display_format', 'short-note'),
            ('link', url),
            ('titleless', True),
        ], post_date)
        swm.assert_called_with(url)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.headers['Location'], url)

    def test_photo_upload(self):
        content = 'content of blog post'
        mock_photo = MockCGIFieldStorage(
            BytesIO(b'photobinarycontent'),
            'filename.jpg')
        request = auth_request(post={
            'h': 'entry',
            'content': content,
            'photo[]': mock_photo
        })
        with mock.patch(self.mp_func('generate_new_post_file')) as gnpf, \
                mock.patch(self.mp_func('post_file')), \
                mock.patch(self.mp_func('process_file')), \
                mock.patch(self.mp_func('MediaFiles')) as MF, \
                mock.patch(self.mp_func('send_webmentions')) as swm:
            response = MicropubEndpoint(request)()
            MF.assert_called_with(settings)
        assert False

    def test_reply(self):
        assert False

    def test_bookmark(self):
        assert False

    def test_syndicated(self):
        assert False

    def test_syndicate_to(self):
        assert False

    def test_send_webmentions(self):
        assert False


class TestMediaParams(TestCase):
    def test_multiple_post_upload(self):
        assert False

    def test_json_photo(self):
        assert False

    def test_multiple_photo_url(self):
        assert False

    def test_video_upload(self):
        assert False


class TestSyndication(TestCase):

    def test_get_syndication(self):
        assert False

    def test_syndicate(self):
        assert False

    def test_format_tweet(self):
        url = "https://example.com/path/path2/"
        title = "This is my long title about a post I will write someday yes!"
        content = "These are the contents, These are the contents, and more..."
        tw = micropub.format_tweet(title, content, url)
        r = [title, "Read more (10 words)...", url]
        self.assertEqual(tw, "\n".join(r))

        title = ""
        content = "These are the contents, These are the contents, and more!"
        content = " ".join([content] * 10)
        tw = micropub.format_tweet(title, content, url)
        magic_trim = 241  # Size that `do_truncate` will return (sans ellipsis)
        self.assertEqual(tw, "{}... {}".format(content[:magic_trim], url))


class TestUpdatePost(TestCase):
    pass


def test_display_format():
    assert False


def test_to_plain_text():
    assert False


class TestSyndicate(TestCase):
    pass


class TestReadLinks(TestCase):
    pass


class TestMediaEndpoint(TestCase):
    pass
