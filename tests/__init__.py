import tempfile
from os import mkdir
from os.path import join

from pelican.settings import DEFAULT_CONFIG

from divebar.pelecanus import settings, HARD_SETTINGS


def test_settings():
    _base = tempfile.TemporaryDirectory()
    OUTPUT_PATH = join(_base.name, "OUTPUT_PATH")
    CONTENT_PATH = join(_base.name, "CONTENT_PATH")
    MEDIA_UPLOADS = join(_base.name, "MEDIA_UPLOADS")
    MEDIA_RECENT_FILES = tempfile.TemporaryFile(dir=_base.name)

    mkdir(OUTPUT_PATH)
    mkdir(CONTENT_PATH)
    mkdir(MEDIA_UPLOADS)

    TEST_SETTINGS = {
        'SITEURL': 'http://blog.localhost',
        'API_HOST': 'http://localhost',
        'ACCESS_TOKEN_SECRET': 'qwerty',
        'MEDIA_RECENT_FILES': MEDIA_RECENT_FILES.name,
        'MEDIA_UPLOADS': MEDIA_UPLOADS,
        'SALT_LENGTH': 5,
        'OUTPUT_PATH': OUTPUT_PATH,
        'PATH': CONTENT_PATH,
        'MEDIA_URL': '/media/',
        "DEFAULT_PAGINATION": 5,
        "THEME": "/home/jj/code/jj.isgeek.net/theme",
        "TWITTER_USER": "TestUser"
    }

    settings.update(DEFAULT_CONFIG)
    settings.update(HARD_SETTINGS)
    settings.update(TEST_SETTINGS)  # So all modules have the same settings
    return settings
