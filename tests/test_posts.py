import tempfile
from os.path import basename, join
from unittest import TestCase

from divebar.core import post_file, process_file
from . import test_settings


settings = test_settings()

SAMPLE_POST = """title: {slug}
date: 2020/05/17 17:42:47
slug: {slug}
status: published

{body}
"""

dated_post = lambda t, m, d, b: """title: {title}
date: 2020/{month:02d}/{month:02d} 17:42:47
slug: {title}
status: published

{body}
""".format(title=t, month=m, day=d, body=b)


class TestPost(TestCase):
    def test_new_post(self):
        md_post = tempfile.NamedTemporaryFile("w")
        md_post.write(SAMPLE_POST.format(slug="xx", body="test_new_post"))
        md_post.seek(0)
        content_file = post_file(md_post.name)
        try:
            md_post.close()
        except:
            pass  # `post_file` deleted the file
        expected = join(settings["PATH"] + "/", "2020/05/{}".format(basename(md_post.name)))
        self.assertEqual(content_file, expected)

        url = process_file(content_file)
        self.assertEqual(url, "2020/05/xx/")

    def test_current_archive_page(self):
        def _new_post(title, _content):
            x_post = tempfile.NamedTemporaryFile("w", suffix=".md")
            x_post.write(SAMPLE_POST.format(slug=title, body=_content))
            x_post.seek(0)
            content_file = post_file(x_post.name)
            try:
                x_post.close()
            except:
                pass  # `post_file` deleted the file
            url = process_file(content_file)
            return url

        # Create N + 1 posts in this period
        items_per_page = settings["DEFAULT_PAGINATION"]
        archive_urls = [_new_post(str(x), str(x)) for x in range(items_per_page)]
        archive_urls.append(_new_post("title", "Extra"))

        archive_page = join(settings["OUTPUT_PATH"], "2020/05/index.html")
        content = open(archive_page).read()
        for url in archive_urls:
            self.assertIn(url, content)

    def test_render_index_on_new_post(self):
        """
        In this test I want to ensure that new posts will re-render
        the homepage correctly and not a shorter version of it.
        Always ensuring the correct posts
        """
        def _new_post(title, month, day, _content):
            x_post = tempfile.NamedTemporaryFile("w", suffix=".md")
            x_post.write(SAMPLE_POST.format(slug=title, body=_content))
            x_post.seek(0)
            content_file = post_file(x_post.name)
            try:
                x_post.close()
            except:
                pass  # `post_file` deleted the file
            url = process_file(content_file)
            return url

        # Create N + 1 posts in this period
        items_per_page = settings["DEFAULT_PAGINATION"]
        archive_urls = [_new_post(str(x), str(x)) for x in range(items_per_page)]
        archive_urls.append(_new_post("title", "Extra"))

        archive_page = join(settings["OUTPUT_PATH"], "2020/05/index.html")
        content = open(archive_page).read()
        for url in archive_urls:
            self.assertIn(url, content)
        assert False
