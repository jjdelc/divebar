from datetime import datetime

from pelican.contents import Article

from divebar.renderers import year_cal, month_archive


def test_year_cal():
    months = [
        ("12", 8),
        ("11", 0),
        ("06", 1000),
        ("05", 0),
        ("04", 0),
        ("03", 1),
        ("02", 45),
        ("01", 58),
    ]
    cal_rows = year_cal(months)
    assert cal_rows == [
        [
            {'count': 8, 'name': 'Dec', 'number': 12, 'pos': '12', "active": True},
            {'count': 0, 'name': 'Nov', 'number': 11, 'pos': '11', "active": False},
            {'count': 0, 'name': 'Oct', 'number': 10, 'pos': '10', "active": False},
            {'count': 0, 'name': 'Sep', 'number': 9, 'pos': '09', "active": False}
        ], [
            {'count': 0, 'name': 'Aug', 'number': 8, 'pos': '08', "active": False},
            {'count': 0, 'name': 'Jul', 'number': 7, 'pos': '07', "active": False},
            {'count': 1000, 'name': 'Jun', 'number': 6, 'pos': '06', "active": True},
            {'count': 0, 'name': 'May', 'number': 5, 'pos': '05', "active": False}
        ], [
            {'count': 0, 'name': 'Apr', 'number': 4, 'pos': '04', "active": False},
            {'count': 1, 'name': 'Mar', 'number': 3, 'pos': '03', "active": True},
            {'count': 45, 'name': 'Feb', 'number': 2, 'pos': '02', "active": True},
            {'count': 58, 'name': 'Jan', 'number': 1, 'pos': '01', "active": True}
        ]
    ]

    months_2 = [
        ("03", 1),
        ("02", 2),
        ("01", 3),
    ]
    cal_rows = year_cal(months_2)
    assert cal_rows == [
        [
            {'count': 0, 'name': 'Apr', 'number': 4, 'pos': '04', "active": False},
            {'count': 1, 'name': 'Mar', 'number': 3, 'pos': '03', "active": True},
            {'count': 2, 'name': 'Feb', 'number': 2, 'pos': '02', "active": True},
            {'count': 3, 'name': 'Jan', 'number': 1, 'pos': '01', "active": True}
        ]
    ]


def test_month_archive():
    arts = [
        Article("", {"title": "post 1", "date": datetime(1970, 1, 1)}),
        Article("", {"title": "post 2", "date": datetime(1970, 1, 1)}),
        Article("", {"title": "post 3", "date": datetime(1970, 1, 1)}),
        Article("", {"title": "post 4", "date": datetime(1980, 2, 2)}),
        Article("", {"title": "post 5", "date": datetime(1980, 2, 2)}),
        Article("", {"title": "post 6", "date": datetime(1990, 3, 3)}),
    ]
    r = month_archive(arts)
    assert r == [
        (datetime(1990, 3, 3).date(), [arts[5]]),
        (datetime(1980, 2, 2).date(), [arts[3], arts[4]]),
        (datetime(1970, 1, 1).date(), [arts[0], arts[1], arts[2]]),
    ]

