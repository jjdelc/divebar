import json
import string
from hashlib import md5
from random import sample
from urllib.error import HTTPError
from urllib.parse import urlencode
from urllib.request import Request, urlopen

from pyramid.httpexceptions import HTTPBadRequest, HTTPForbidden, \
    HTTPUnauthorized, HTTPMethodNotAllowed
from pyramid.response import Response

from divebar.pelecanus import settings


INDIEAUTH_CODE_CHECK = "https://indieauth.com/auth"


def get_access_token(settings, scope):
    """
    Returns an access token, seemingly random that doesn't need to be persisted.

    This way we will validate tokens that we know are generated through this
    generator function.
    """
    s_length = settings['SALT_LENGTH']
    salt = ''.join(sample(string.ascii_letters, s_length)).encode('utf-8')
    salt = md5(salt).hexdigest()[:s_length]
    secret = '%s%s%s' % (salt, settings['ACCESS_TOKEN_SECRET'], scope)
    secret = md5(secret.encode('utf-8')).hexdigest()
    return '%s%s' % (salt, secret[s_length:])


def verify_access_token(settings, token, scope):
    if not token:
        return False
    s_length = settings['SALT_LENGTH']
    salt = token[:s_length]
    secret = token[s_length:]
    expected_secret = '%s%s%s' % (salt, settings['ACCESS_TOKEN_SECRET'], scope)
    expected_secret = md5(expected_secret.encode('utf-8')).hexdigest()
    return secret == expected_secret[s_length:]


def assert_authenticated(request):
    if 'Authorization' in request.headers:
        bearer = request.headers['Authorization']
        token = bearer.replace('Bearer ', '')
    else:
        token = request.params.get('access_token')
    scope = ''  # For backwards compatibility with current tokens
    if not verify_access_token(settings, token, scope):
        raise HTTPUnauthorized()


def check_indieauth(code, client_id, redirect_uri):
    req = Request(INDIEAUTH_CODE_CHECK, urlencode({
        'code': code,
        'client_id': client_id,
        'redirect_uri': redirect_uri
    }).encode('utf-8'), {
        'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'Accept': 'application/json'
    })
    try:
        resp = urlopen(req)
    except HTTPError:
        raise HTTPForbidden("Invalid auth domain")

    if resp.status == 200:
        payload = resp.read()
        payload = json.loads(payload.decode('utf-8'))
    else:
        raise HTTPBadRequest("Bad code")
    if payload.get('me') not in settings['AUTH_SITES']:
        raise HTTPForbidden("Invalid auth domain")
    return payload


def token_endpoint(request):
    if request.method == 'OPTIONS':
        Response(status=200, headerlist=[
            ('Access-Control-Allow-Origin', '*'),
            ('Access-Control-Allow-Headers', 'Authorization'),
            ('Access-Control-Allow-Credentials', 'true'),
            ('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        ])

    if request.method != 'POST':
        raise HTTPMethodNotAllowed()

    try:
        code = request.POST['code']
        client_id = request.POST['client_id']
        redirect_uri = request.POST['redirect_uri']
    except KeyError as e:
        raise HTTPBadRequest()

    auth = check_indieauth(code, client_id, redirect_uri)

    response = Response()
    response.json = {
        'me': auth['me'],
        'access_token': get_access_token(settings, ''),
        'scope': auth['scope']
    }
    response.headerlist.extend([
        ('Access-Control-Allow-Origin', '*')
    ])
    return response
