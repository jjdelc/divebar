import json
import logging
from urllib.parse import urlparse, urljoin

import requests
from bs4 import BeautifulSoup
from mf2py import Parser
from pyramid.httpexceptions import HTTPBadRequest, HTTPMethodNotAllowed
from pyramid.response import Response
from redis import Redis
from rq import Queue

from divebar.api.media import MediaFiles
from divebar.core import html_index_for_url, source_file_for_url, \
    read_post_meta, update_content, process_file, post_file
from divebar.db import Notifications
from divebar.pelecanus import settings


logging.basicConfig(level=logging.INFO)


def get_queue():
    redis_conn = Redis(**settings['RQ_REDIS'])
    return Queue('divebar', connection=redis_conn)


def is_absolute(url):
    return bool(urlparse(url).netloc)


def links_in_content(contents=None, doc=None, section=None):
    """
    We allow to receive an extra `section` argument because for the case of
    incoming webmentions we need to parse the links of the source's h-entry
    first and not its e-content.

    In the case of outgoing mentions, we will parse our e-content preferably.
    """
    doc = doc or BeautifulSoup(contents, 'html5lib')
    article = doc.find(class_=section or 'e-content')
    article = article or doc.find(class_='h-entry')
    if not article:
        return set()
    links = {a.attrs['href'] for a in article.select('a[href]')
             if a.attrs.get('href', '').startswith(('http://', 'https://'))}
    return links


def meta_shared_links(meta):
    urls = set()
    for k, v in meta:
        if k in {'like_of', 'rsvp_of', 'bookmark_of', 'reply_to'}:
            try:
                url = json.loads(v).get('url')
            except (TypeError, ValueError) as err:
                continue
            if url:
                urls.add(url)
    return urls


def find_post_links(url):
    """Given a local URL, find all the links on its e-content"""
    url = urlparse(url).path if is_absolute(url) else url
    index_file = html_index_for_url(url)
    source_file = source_file_for_url(url)
    post_meta = read_post_meta(source_file)
    shared_links = meta_shared_links(post_meta)
    contents = open(index_file).read()
    return links_in_content(contents) | shared_links


def _send_webmentions(post_url):
    """
    Given a post URL of this blog, find the links on the post contents and send
    the webmentions for those links found
    """
    logging.info("Starting mentions for {}".format(post_url))
    links = find_post_links(post_url)
    to_mention = {l for l in links
                  if urlparse(l).netloc not in settings['WEBMENTIONS_SKIP_DOMAINS']}
    for url in to_mention:
        webmention(post_url, url)
    logging.info("Done with {} mentions on {}".format(len(to_mention), post_url))


def find_webmention_endpoint(url):
    """
    Discovers the webmention endpoint for a URL, returns None if not found
    """
    try:
        resp = requests.get(url)
    except Exception:
        return None
    if resp.status_code != 200:
        return None

    if 'webmention' in resp.links:
        wm = resp.links['webmention']['url']
    elif any('webmention' in k for k in resp.links):
        wm = None
        for k, v in resp.links.items():
            if 'webmention' in k.split(' '):
                wm = v['url']
    else:
        page_content = resp.text
        parsed = Parser(doc=page_content, html_parser='html5lib').to_dict()
        wm = parsed['rels'].get('webmention') or []
        wm = list(reversed(sorted(wm)))  # Prefer the longer URL
        wm = wm[0] if wm else None
    if wm is None:
        return None
    return wm if is_absolute(wm) else urljoin(url, wm)


def _webmention(source, target):
    endpoint = find_webmention_endpoint(target)
    if not endpoint:
        logging.info("No webmention endpoint found for {}".format(target))
        return

    logging.info("Sending mention from {} to {}".format(source, endpoint))
    requests.post(endpoint, {
        'target': target,
        'source': source
    })
    logging.info("Mention sent from {} to {}".format(source, target))


def webmention(source, target):
    logging.info('Queued mentions from {} to {}'.format(source, target))
    return get_queue().enqueue(_webmention, args=(source, target))


def send_webmentions(post_url):
    """
    Asynchronously send webmentions parsing and sending
    """
    logging.info('Queued mentions for {}'.format(post_url))
    return get_queue().enqueue(_send_webmentions, args=[post_url])


def webmention_endpoint(request):
    if request.method == 'POST':
        logging.info("Webmention requested")
        try:
            source = request.POST['source']
            target = request.POST['target']
        except KeyError:
            logging.error("Bad request keys")
            raise HTTPBadRequest()

        validate_mention(source, target)
        receive_webmention(source, target)
        return Response(status=202, headerlist=[
            ('Access-Control-Allow-Origin', '*'),
        ])
    raise HTTPMethodNotAllowed()


VALID_SCHEMES = ('http', 'https')


def validate_mention(source, target):
    if source == target:
        # Following spec, we should reject if they're the same
        logging.error("Target same as source???")
        raise HTTPBadRequest()

    target_parsed = urlparse(target)

    # Per spec, check that both urls are valid schemes. This means that
    # we are rejecting links to our site that are not absolute.
    if target_parsed.scheme not in VALID_SCHEMES:
        logging.error("Invalid scheme on target")
        raise HTTPBadRequest()
    if not source.startswith(VALID_SCHEMES):
        logging.error("Invalid scheme on source")
        raise HTTPBadRequest()

    if target_parsed.path == '/':
        logging.error("Cannot mention the index page")
        raise HTTPBadRequest()

    target_host = target_parsed.netloc
    if target_host != urlparse(settings['SITEURL']).netloc:
        # Make sure that the link goes to the blog's domain and not to some
        # other domain
        logging.error("Target sent to wrong blog {} vs {}".format(target, target_host))
        raise HTTPBadRequest()
    logging.info("Valid webmention")


def receive_webmention(source, target):
    return get_queue().enqueue(_receive_webmention, args=[source, target])


def _receive_webmention(source, target):
    try:
        resp = requests.get(source)
    except Exception:
        logging.error("Cannot read source: {}".format(source))
        return  # Cannot read source, ignore mention

    if resp.status_code == 410:
        pass   # TODO: Delete mention?

    if resp.status_code != 200:
        logging.error("Error fetching source {}: {}".format(resp.status_code, source))
        return  # Error, ignore mention

    links = links_in_content(resp.text, section='h-entry')
    # The link, exactly as it's been sent will be matched agaist the source's
    # links. Keep relative of absolute URL
    if target not in links:
        logging.error('{} did not link to {}'.format(source, target))
        return   # Page does not contain link

    # Success, page contains link
    save_mentions(source, target)


def mentions_for_meta(meta):
    mentions = []
    for attr, val in meta:
        if attr == 'mentions':
            try:
                _m = json.loads(val)
                _m = _m if isinstance(_m, list) else _m
                mentions.extend(_m)
            except ValueError:
                logging.error("Bad mentions meta: {}".format(val))
                pass
    return mentions


def find_mention(source, target):
    """
    Returns a JSON representation of the webmention
    """
    try:
        resp = requests.get(source)
    except Exception:
        logging.error("Failed to read source {}".format(source))
        return None

    if resp.status_code == 410:
        return  # Remove mention?

    if resp.status_code != 200:
        logging.info("Source {} returned {}".format(source, resp.status_code))
        return

    page_content = resp.text
    doc = BeautifulSoup(page_content, 'html5lib')
    links = links_in_content(doc=doc, section='h-entry')

    if target not in links:
        logging.error("Mention not found in page {}".format(source))
        return

    parsed = Parser(doc=doc, html_parser='html5lib').to_dict()
    entry = [e for e in parsed['items'] if e['type'][0] == 'h-entry']
    entry = entry[0] if entry else None
    mention = {'source': source}

    if not entry:
        mention['title'] = doc.title.text
        mention['url'] = source
    else:
        props = entry['properties']
        published = props.get('published', [None])[0]
        title = props.get('name', [doc.title.text])[0]
        url = props.get('url', [source])[0]
        is_rt = "/repost/" in source
        is_like = 'like-of' in props
        is_rsvp = 'rsvp-of' in props
        author = props_author(props)
        mention['title'] = title
        mention['url'] = url
        if published:
            mention['published'] = published
        if author:
            mention['author'] = author
        if is_like:
            mention['is_like'] = True
        if is_rsvp:
            mention['is_rsvp'] = True
        if is_rt:
            mention['is_rt'] = True
        if url != source:
            mention['via'] = urlparse(source).netloc
    logging.info("Mention found on {}".format(source))
    return mention


def props_author(props):
    if 'author' not in props:
        return
    try:
        author = props['author'][0]['properties']
    except (KeyError, IndexError):
        return
    if not author:
        return
    photo_url = author.get('photo', [None])[0]
    if photo_url:
        media = MediaFiles(settings)
        photo_url = media.store_url(photo_url, author["name"])
    return {
        'name': author.get('name', [None])[0],
        'photo': photo_url,
        'url': author.get('url', [None])[0]
    }


def extend_mentions(mentions, new_mention):
    """
    Since mentions are unique per URL, if we get a new mention from an
    existing url, we should overwrite it instead of appending
    """
    url = new_mention['url']
    _mentions = []
    added = False
    for m in mentions:
        if m['url'] != url:
            _mentions.append(m)
        else:
            _mentions.append(new_mention)
            added = True
    if not added:
        _mentions.append(new_mention)
    return _mentions


def save_mentions(source, target):
    # We want only the path
    source_file = source_file_for_url(target)
    if not source_file:
        logging.error("Not source file for {}".format(target))
        return
    meta = read_post_meta(source_file)
    mentions = mentions_for_meta(meta)
    new_mention = find_mention(source, target)
    if not new_mention:
        logging.error("No mention found")
        return
    logging.info("Saving mentions for {}".format(target))
    mentions = extend_mentions(mentions, new_mention)
    update_content(source_file, [
        ('mentions', json.dumps(mentions))
    ])
    logging.info("Saved to {}".format(source_file))
    final = post_file(source_file)
    process_file(final)
    logging.info("Webmention to {} successful!".format(target))
    Notifications(settings["DB"]).add_notification("webmention", target, new_mention)
