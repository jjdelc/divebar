import logging
from io import BytesIO
from os.path import splitext, join
from urllib.parse import urljoin, urlparse
from urllib.request import urlopen

from hashfs import HashFS
from pyramid.httpexceptions import HTTPMethodNotAllowed
from pyramid.response import Response

from divebar.api.auth import assert_authenticated
from divebar.db import PicturesDB
from divebar.pelecanus import settings


logging.basicConfig(level=logging.INFO)


class MediaFiles:
    def __init__(self, config):
        self.config = config
        self.uploads_path = config['MEDIA_UPLOADS']
        self.fs = HashFS(self.uploads_path, depth=2, width=2)
        self.media_url = config['MEDIA_URL']
        self.db = PicturesDB(config["DB"])

    def get_urls(self, total=12):
        return [self.urlize(r["url"]) for r in self.db.recent(total)]

    def urlize(self, address):
        return urljoin(self.media_url, address)

    def append(self, files):
        for url in files:
            self.db.add_picture(url)

    def store_bytes(self, file_bytes, ext, append_log=True):
        address = self.fs.put(file_bytes, ext)
        path = address.relpath
        if append_log:
            self.append([path])
        return self.urlize(path)

    def store(self, files):
        uploads = []
        for _file in files:
            if type(_file) is bytes and len(_file) is 0:
                continue
            # Split by ? because it can be an URL with GET params
            filename, ext = splitext(_file.filename.split('?')[0])
            address = self.fs.put(_file.file, ext)
            uploads.append(address.relpath)

        self.append(uploads)
        return [self.urlize(ad) for ad in uploads]

    def store_url(self, url, name):
        name = name[0] if isinstance(name, list) else name
        logging.info("Saving avatar for: {}".format(name))
        try:
            logging.info("Reading avatar from: {}".format(url))
            readfile = BytesIO(urlopen(url).read())
        except:
            # Failed to read URL, return same
            logging.exception("Failed reading url: {}".format(url))
        else:
            url = self.store_bytes(readfile, "jpg", append_log=False)
            logging.info("Saved as: {}".format(url))
        return url

    def read(self, url):
        parsed = urlparse(url)
        path = parsed.path
        full_path = join(self.uploads_path, path.lstrip('/'))
        return self.fs.open(full_path)


def media_endpoint(request):
    if request.method != 'OPTIONS':
        assert_authenticated(request)

    media_files = MediaFiles(settings)

    if request.method == 'GET':
        response = Response()
        response.json = media_files.get_urls()
        response.headerlist.extend([
            ('Access-Control-Allow-Origin', '*')
        ])
        return response
    elif request.method == 'POST':
        files = request.POST.getall('file')
        urls = media_files.store(files)
        response = Response(status=201)
        response.headerlist.extend([
            ('Access-Control-Allow-Origin', '*')
        ])
        response.headerlist.extend([
            ('Location', _u) for _u in urls[:1]  # Only one Location header
        ])
        return response
    elif request.method == 'OPTIONS':
        return Response(status=200, headerlist=[
            ('Access-Control-Allow-Origin', '*'),
            ('Access-Control-Allow-Headers', 'Authorization'),
            ('Access-Control-Allow-Credentials', 'true')
        ])
    else:
        raise HTTPMethodNotAllowed()
