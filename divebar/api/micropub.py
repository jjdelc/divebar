import json
import logging
from io import BytesIO
from datetime import datetime
from urllib.parse import urljoin, urlparse, unquote

import twitter
import dateutil.parser
from bs4 import BeautifulSoup
from jinja2.filters import do_wordcount, do_truncate
from markdown import markdown
from pyramid.response import Response
from twitter.error import TwitterError
from webob.multidict import MultiDict
from pyramid.httpexceptions import HTTPBadRequest, HTTPMethodNotAllowed

from divebar.api.media import MediaFiles
from divebar.api.auth import assert_authenticated
from divebar.api.webmention import send_webmentions
from divebar.api.readlinks import read_link, twitter_embed
from divebar.core import make_title, generate_new_post_file, post_file, \
    process_file, source_file_for_url, read_post_meta, update_content, build_url
from divebar.pelecanus import settings
from divebar.pelecanus.parsers import extract_hashtags


logging.basicConfig(level=logging.INFO)

TWEET_MAX_CHARS = 280
OPTS_CORS_RESP = Response(status=200, headerlist=[
    ('Access-Control-Allow-Origin', '*'),
    ('Access-Control-Allow-Headers', 'Authorization'),
    ('Access-Control-Allow-Credentials', 'true'),
    ('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
])


def categories_from_content(content):
    hashtags = extract_hashtags(content)
    return [("categories", ht.strip("#")) for ht in hashtags]


def prepare_params(_settings, url, content, has_title, data, media_files):
    """

    :param _settings: Settings for this instalation
    :param url: URL for reply-to, bookmark, like, etc.
    :param content: Body of the post
    :param has_title: Boolean if this is a full blog, it might have a title
    :param data:
    :param media_files: MediaFiles() instance in case there's a photo to upload
    :return: (post attrs, extra actions)
    """
    multi_params = []  # Like a dict, but this way can store multiple values per key
    unique_params, link_data = {'link': url}, {}
    reply_to_url, mark_favorite, param_name = None, None, None
    uploaded_media_params, post_photo = media_params(data, media_files)

    if uploaded_media_params:
        multi_params.extend(uploaded_media_params)

    if 'in-reply-to' in data:
        link_data = read_link(data['in-reply-to'])
        param_name = "reply_to"
        has_title = False
        reply_to_url = link_data['url']

    if 'bookmark-of' in data:
        link_data = read_link(data['bookmark-of'])
        param_name = "bookmark_of"
        has_title = False

    if 'like-of' in data:
        link_data = read_link(data['like-of'])
        param_name = "like_of"
        has_title = False
        mark_favorite = link_data['url']

    if 'rsvp-of' in data:
        link_data = read_link(data['rsvp-of'])
        param_name = "rsvp_of"
        has_title = False
        mark_favorite = link_data['url']

    if link_data.get("twitter"):
        # If the URL we're replying to is a Tweet, obtain that tweet's embed
        # information so the post can render it.
        api = twitter_api(_settings)
        link_data.update(twitter_embed(api, link_data["url"]))

    if param_name:
        unique_params[param_name] = json.dumps(link_data)

    if not has_title:
        unique_params['titleless'] = True
    if 'location' in data:
        unique_params['location'] = data['location']
    if 'place_name' in data:
        unique_params['place_name'] = data['place_name']

    if 'syndication' in data:
        multi_params.extend([('syndication', st)
                             for st in data.getall('syndication')])
    if 'category[]' in data:
        multi_params.extend([('categories', st)
                             for st in data.getall('category[]')])

    # Add categories from hastags in content's body
    multi_params.extend(categories_from_content(content))

    syndicate_to = data.pop('mp-syndicate-to',
                            data.pop('mp-syndicate-to[]', []))

    unique_params['display_format'] = get_display_format(content,
        has_title, multi_params)

    params = sorted(unique_params.items()) + multi_params

    # Actions are instructions of what other actions related with other
    # services around this post being published. Some are needed before
    # (like syndication so we can render the URLs) and some happen after
    # (like webmentions on replies)
    actions = {
        'reply_to_url': reply_to_url,
        'mark_favorite': mark_favorite,
        'syndicate_to': syndicate_to,
        'post_photo': post_photo
    }

    return params, actions


def syndicate_post(post, _settings):
    if not post.syndicate_to:
        logging.info("Nothing to syndicate")
        return
    logging.info("Request to syndicate to {}".format(', '.join(post.syndicate_to)))
    tw_title = post.title if post.has_title else ''
    try:
        syndication_urls = syndicate(post.syndicate_to, _settings,
                                     tw_title, post.content, post.url,
                                     post.post_photo, post.reply_to_url)
    except TwitterError as err:
        # This usually means duplicate status. Which means a double
        # post, which we also don't want in the blog.
        # works as a safety measure as well.
        # This aborts before creating any post file, so nothing
        # was altered in the world. Let the client handle the error.

        # TODO: delete post file
        logging.error("Failed to syndicate to Twitter")  # TwitterError
        logging.error(err.message)
        raise HTTPBadRequest(err.message)

    # After successful syndication, update the post metadata
    # before processing it out to generate the blog
    update_content(post.source_file, [
        ('syndication', st) for st in syndication_urls
    ])


class Post:
    def __init__(self, url, title, slug, has_title,  content, post_date, params, actions):
        self.content = content
        self.url = url
        self.title = title
        self.slug = slug
        self.has_title = has_title
        self.post_date = post_date
        self.params = params
        self.mark_favorite = actions['mark_favorite']
        self.reply_to_url = actions['reply_to_url']
        self.post_photo = actions['post_photo']
        syndicate_to = actions['syndicate_to']
        syndicate_to = syndicate_to if isinstance(syndicate_to,
                                                  list) else [syndicate_to]
        self.syndicate_to = syndicate_to

    def generate_source(self):
        self.source_file = generate_new_post_file(self.title, self.slug,
                                                  self.content, self.params,
                                                  self.post_date)

    def publish(self):
        final = post_file(self.source_file)
        process_file(final)

    def perform_pre_actions(self, _settings):
        """
        Performs the syndicating actions to other services/servers that are
        needed before publishing the post.
        """
        if self.syndicate_to:
            syndicate_post(self, _settings)
        if self.mark_favorite:
            syndicate_favorite(self.mark_favorite, _settings)


def mp_new_entry(data, _settings):
    if 'published' in data:
        post_date = data.pop('published')
        try:
            post_date = dateutil.parser.parse(post_date)
        except ValueError:
            raise HTTPBadRequest('Invalid publish date')
    else:
        post_date = datetime.now()

    title = data.pop('name', None)
    title, slug, has_title = make_title(title, post_date)

    url = build_url(post_date, slug)
    url = urljoin(_settings['SITEURL'], url)
    logging.info("Received {}".format(url))

    content = data.pop('content', '') or ''
    media_files = MediaFiles(_settings)
    params, actions = prepare_params(_settings, url, content, has_title, data, media_files)

    post = Post(
        url,
        title,
        slug,
        has_title,
        content,
        post_date,
        params,
        actions
    )
    logging.info("Generating post: {}".format(url))
    post.generate_source()
    post.perform_pre_actions(_settings)
    post.publish()

    logging.info("File processed")
    return post.url


class MicropubEndpoint:
    def __init__(self, request):
        self.request = request

    def __call__(self):
        if self.request.method == 'OPTIONS':
            return OPTS_CORS_RESP

        assert_authenticated(self.request)
        try:
            method = {
                'GET': self.GET,
                'POST': self.POST,
            }[self.request.method]
        except KeyError:
            raise HTTPMethodNotAllowed()
        return method()

    def mp_config(self):
        return {
            'media-endpoint': urljoin(settings['API_HOST'], '/media'),
            'syndicate-to': get_syndication(settings)
        }

    def mp_syndicate_to(self):
        return {
            'syndicate-to': get_syndication(settings)
        }

    def mp_source(self, data):
        url = data.get('url')
        if not url:
            raise HTTPBadRequest()

        # Read attributes and post's meta to return
        try:
            source_file = source_file_for_url(url)
        except FileNotFoundError:
            try:  # Now try in case the url was escaped ...
                source_file = source_file_for_url(unquote(url))
            except FileNotFoundError:
                raise HTTPBadRequest("Invalid post URL")
        data = dict(read_post_meta(source_file, include_content=True))
        properties = data.get('properties') or []
        if not properties:
            payload = {
                'type': ['h-entry'],
                'properties': {
                    'published': [data['date'].isoformat()],
                    'name': [data['title']],
                    'content': [data['content']]
                }
            }
        else:
            # Need to implement returning only selected properties
            raise NotImplementedError()
        return payload

    def GET(self):
        data = MultiDict(self.request.GET)
        q = data.get('q')
        if q == 'config':
            payload = self.mp_config()
        elif q == 'syndicate-to':
            payload = self.mp_syndicate_to()
        elif q == 'source':
            payload = self.mp_source(data)
        else:
            raise HTTPBadRequest("Invalid query: {}".format(q))

        response = Response()
        response.json = payload
        response.headerlist.extend([
            ('Access-Control-Allow-Origin', '*')
        ])
        return response

    def mp_action(self, data):
        action = data['action']
        url = data.get('url')
        if not url:
            raise HTTPBadRequest("Please provide a post url to edit")

        try:
            source_file = source_file_for_url(url)
        except FileNotFoundError:
            raise HTTPBadRequest("No post found for {}".format(url))

        if action == 'update':
            replace = data['replace']
            attrs = [
                ('content', replace['content'][0]),
                ('title', replace['name'][0]),
            ]
            source_file = update_content(source_file, attrs)
            final = post_file(source_file)
            process_file(final)
            send_webmentions(url)
        else:
            raise HTTPBadRequest("Not implemented yet")

        return Response(status=204, headerlist=[
            ('Access-Control-Allow-Origin', '*'),
        ])

    def new_post(self, data):
        new_post_url = mp_new_entry(data, settings)
        # Send webmentions for the new post.
        send_webmentions(new_post_url)
        return Response(status=201, headerlist=[
            ('Access-Control-Allow-Origin', '*'),
            ('Access-Control-Expose-Headers', 'Location'),
            ('Location', new_post_url)
        ])

    def POST(self):
        data = MultiDict(self.request.POST or self.request.json_body)
        if 'action' in data:
            return self.mp_action(data)
        elif data.get('h') == 'entry':
            return self.new_post(data)
        else:
            raise HTTPBadRequest()


def get_display_format(content, has_title, m_params):
    has_photo = 'photo_url' in set(zip(*m_params))
    short_body = len(content) < 300

    display_format = 'entry'
    if not has_title and has_photo:
        display_format = 'entry'
    elif not has_title and short_body:
        display_format = 'short-note'

    return display_format


def twitter_api(_settings):
    return twitter.Api(
        consumer_key=_settings['TWITTER_CONSUMER_KEY'],
        consumer_secret=_settings['TWITTER_CONSUMER_SECRET'],
        access_token_key=_settings['TWITTER_ACCESS_TOKEN'],
        access_token_secret=_settings['TWITTER_ACCESS_SECRET']
    )


def get_syndication(_settings):
    syndication_targets = []
    if _settings.get('TWITTER_CONSUMER_KEY'):
        # Twitter configured (hopefully along all 4 keys)
        api = twitter_api(_settings)
        try:
            user = api.VerifyCredentials()
        except twitter.error.TwitterError:
            # Error verifying credentials. Twitter API changed :(
            pass
        else:
            twitter_handle = user.screen_name
            twitter_service = {
                "uid": "twitter",
                "name": "{} on Twitter".format(user.name),
                "service": {
                    "name": "Twitter",
                    "url": "https://twitter.com/",
                    "photo": "https://twitter.com/favicon.ico"
                },
                "user": {
                    "name": twitter_handle,
                    "url": user.url,
                    "photo": user.profile_image_url
                }
            }
            syndication_targets.append(twitter_service)
    return syndication_targets


def to_plain_text(md_content):
    html = markdown(md_content)
    text = ''.join(BeautifulSoup(html, 'html.parser').find_all(text=True))
    return text.strip()


def format_tweet(title, content, url):
    if title:
        word_count = "Read more ({} words)...".format(do_wordcount(content))
        chars_left = TWEET_MAX_CHARS - len(url) - len(word_count) - 5
        tweet = "{}\n{}\n{}".format(title[:chars_left], word_count, url)
    else:
        tweet = title or to_plain_text(content)[:TWEET_MAX_CHARS]
        _tweet = '{} {}'.format(tweet, url)
        if len(_tweet) > TWEET_MAX_CHARS:  # Only trim if needed
            content_size = TWEET_MAX_CHARS - len(url) - 3
            tweet = do_truncate(None, tweet, content_size, leeway=0)
            tweet = '{} {}'.format(tweet, url)
        else:
            tweet = _tweet
    assert len(tweet) <= TWEET_MAX_CHARS
    return tweet


def syndicate(targets, _settings, title, content, url, media, reply_to_url):
    urls = []
    if 'twitter' in targets:
        api = twitter_api(_settings)
        reply_tweet_id = None
        if reply_to_url:
            parsed_reply_url = urlparse(reply_to_url)
            if 'twitter.com' == parsed_reply_url.netloc:
                reply_tweet_id = parsed_reply_url.path.split('/')[-1]
        tweet = format_tweet(title, content, url)
        status = api.PostUpdate(tweet[:TWEET_MAX_CHARS], media=media,
                                in_reply_to_status_id=reply_tweet_id,
                                auto_populate_reply_metadata=bool(reply_tweet_id))
        status_url = 'https://twitter.com/{}/status/{}'.format(
            status.user.screen_name, status.id
        )
        logging.info("Posted to twitter: {}".format(status_url))
        urls.append(status_url)
    return urls


def syndicate_favorite(url, _settings):
    parsed_fav_url = urlparse(url)
    if 'twitter.com' == parsed_fav_url.netloc:
        logging.info("Marking like on twitter: {}".format(url))
        tweet_id = parsed_fav_url.path.split('/')[-1]
        api = twitter_api(_settings)
        api.CreateFavorite(status_id=tweet_id)
        logging.info("Favorited tweet: {}".format(tweet_id))


def read_photo(photo):
    """
    Given that we already read the pictures to save them in MediaStorage
    when making this call, seek it back to 0 and ensure it has a .name and .rb
    because the Twitter python API will want those attributes.
    """
    post_photo = photo.file
    post_photo.seek(0)
    post_photo = BytesIO(post_photo.read())
    post_photo.name = photo.filename
    post_photo.mode = 'rb'
    return post_photo


def media_params(data, media_files):
    """
    If there's media information in the post `data` store and return the
    URLs for this newly stored media and the File object of the new photo.
    """
    params = []
    post_photo = None

    if 'photo' in data or 'photo[]' in data:
        p_key = 'photo[]' if 'photo[]' in data else 'photo'
        photo_files = data.getall(p_key)

        # In case a client used the Media endpoint to store the pictures first
        # instead of posting the file itself.
        if all(isinstance(_u, str) for _u in photo_files):
            urls = photo_files
            post_photo = media_files.read(photo_files[0])
        else:
            urls = media_files.store(photo_files)
            post_photo = read_photo(photo_files[0])

        params.extend([('photo_url', _u) for _u in urls])
        logging.info("Found {} pictures in request".format(len(photo_files)))

    if 'video' in data or 'video[]' in data:
        v_key = 'video[]' if 'video[]' in data else 'video'
        video_files = data.getall(v_key)

        if all(isinstance(_u, str) for _u in video_files):
            urls = video_files
        else:
            urls = media_files.store(video_files)

        params.extend([('video_url', _u) for _u in urls])
        logging.info("Found {} videos in request".format(len(video_files)))

    return params, post_photo
