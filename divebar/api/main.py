from pyramid.config import Configurator

from divebar.api.auth import token_endpoint
from divebar.api.media import media_endpoint
from divebar.api.micropub import MicropubEndpoint
from divebar.api.webmention import webmention_endpoint


def get_app():
    config = Configurator()
    config.add_route('new_micropub', '/micropub')
    config.add_route('token_endpoint', '/token')
    config.add_route('media_endpoint', '/media')
    config.add_route('webmention', '/webmention')
    config.add_view(MicropubEndpoint, route_name='new_micropub')
    config.add_view(token_endpoint, route_name='token_endpoint')
    config.add_view(media_endpoint, route_name='media_endpoint')
    config.add_view(webmention_endpoint, route_name='webmention')
    return config.make_wsgi_app()
