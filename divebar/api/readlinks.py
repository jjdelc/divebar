from os.path import basename, split
from urllib.parse import urlparse, parse_qs

import mf2py
import requests
from bs4 import BeautifulSoup
from opengraph.opengraph import OpenGraph

from divebar.api.media import MediaFiles
from divebar.pelecanus import settings


UA = "divebar-embed-parser"


def is_image(url):
    try:
        resp = requests.head(url)
    except Exception:
        # Could not HEAD the url, maybe it is down
        return False
    mime = resp.headers.get("content-type")
    if not mime:
        return False
    return "image" in mime.lower()


def img_name(url):
    parsed = urlparse(url)
    return basename(parsed.path) or parsed.netloc


YOUTUBE_MARKUP = """
<a class="youtube-preview-embed" href="%(youtube_url)s" data-video-id="%(video_id)s"><img src="%(thumbnail_url)s"/></a>
"""

THUMBNAIL_MASK = "https://i1.ytimg.com/vi/%s/0.jpg"


def youtubize(youtube_url):
    p = urlparse(youtube_url)
    if p.netloc in ("www.youtube.com", "youtube.com"):
        if "playlist" in p.path:
            pass
        else:
            video_id = parse_qs(p.query)["v"][0]
    elif p.netloc == "youtu.be":
        video_id = p.path.strip("/")
    thumbnail_url = THUMBNAIL_MASK % video_id
    return {
        "youtube_url": youtube_url,
        "thumbnail_url": thumbnail_url,
        "video_id": video_id,
    }


def is_youtube_video(site_name, url):
    is_yt = site_name == "YouTube" or "youtube.com/" in url or "youtu.be/" in url
    if not is_yt:
        return False
    p = urlparse(url)
    return "v=" in p.query


def read_link(url):
    parsed = {"url": url, "is_image": False}
    if is_image(url):
        parsed["is_image"] = True
        parsed["title"] = img_name(url)
        return parsed

    p = urlparse(url)
    if p.netloc in {"twitter.com", "www.twitter.com"} and len(p.path) > 1:
        # We can't read Twitter's metadata by fetching. We need to get the
        # tweet using the API
        parsed["twitter"] = True
        return parsed
    elif p.netloc in {"reddit.com", "www.reddit.com", "old.reddit.com"} and len(p.path) > 1:
        url = url + ".json"
        parsed["reddit"] = True

    try:
        resp = requests.get(url, headers={"User-Agent": UA})
    except Exception:
        return parsed
    if resp.status_code != 200:
        return parsed

    if "application/json" in resp.headers.get("content-type"):
        payload = resp.json()
        if parsed.get("reddit"):
            parsed.update(parse_reddit(payload, url))
    else:
        html = resp.text
        parsed.update(parse_contents(html, url))
    return parsed


def parse_contents(html, url):
    parsed = {}
    og = OpenGraph(html=html)
    parsed.update(og.__data__.items())
    mf = mf2py.parse(doc=html, html_parser="html5lib")
    parsed["summary"] = parsed.get("summary", parsed.get("description"))
    if len(mf["items"]) > 0:
        props = mf["items"][0]["properties"]
        r_title = props["name"][0]
        parsed["title"] = r_title
        try:
            r_author = props["author"][0]["properties"]["name"][0]
            parsed["author"] = r_author
        except (KeyError, IndexError):
            pass  # No author
    else:
        soup = BeautifulSoup(html, "html5lib")
        if soup.title:
            parsed["title"] = soup.title.text
        if not parsed["summary"]:
            possible_descs = soup.findAll(attrs={"name": "description"})
            if possible_descs:
                desc = possible_descs[0]
                parsed["summary"] = desc.attrs.get("content", "")

    site_name = parsed.get("site_name")
    if site_name == "Twitter":
        if "/profile_images/" in parsed.get("image", ""):
            # This is a Twitter default user avatar
            parsed["author_photo"] = parsed.pop("image")
    elif is_youtube_video(site_name, url):
        parsed["site_name"] = "YouTube"
        parsed["embed_video"] = True
        parsed["youtube"] = youtubize(url)
    return parsed


def twitter_embed(api, url):
    status_id = split(urlparse(url).path.rstrip("/"))[-1]
    tweet = api.GetStatus(status_id)
    author_avatar = tweet.user.profile_image_url_https
    media = MediaFiles(settings)
    author_avatar = media.store_url(author_avatar, tweet.user.screen_name)
    response = {
        "summary": tweet.text,
        "author": {
            "name": tweet.user.name,
            "username": tweet.user.screen_name,
            "url": tweet.user.url,
            "image": author_avatar,
        },
        "site_name": "Twitter"
    }
    if tweet.media:
        response["image"] = tweet.media[0].media_url_https
    return response


def parse_reddit(payload, url):
    parsed = {}
    post = payload[0]["data"]["children"][0]["data"]
    parsed["title"] = post["title"]
    parsed["author"] = {
        "name": post["author"],
        "url": "https://www.reddit.com/u/{}".format(post["author"])
    }
    if post.get("selftext"):
        parsed["summary"] = post["selftext"]
    if "thumbnail" in post:
        parsed["image"] = post["thumbnail"]
    return parsed
