import logging
from datetime import datetime
from email.mime.text import MIMEText
from smtplib import SMTP_SSL

from divebar.db import Notifications
from divebar.pelecanus import settings


logging.basicConfig(level=logging.INFO)

SUBJECT_TPL = "{notification_total} notifications for {date}"


def process_notifications(all_notifications):
    urls = {}
    for notification in all_notifications:
        url = notification["url"]
        if url not in urls:
            urls[url] = []
        payload = notification["payload"]
        verb = "like" if payload.get("is_like", False) else "webmention"
        verb = "rt" if payload.get("is_rt", False) else verb
        verb = verb.capitalize()
        act_name = payload["author"].get("name", "<no-name>")
        act_url = payload["author"].get("url", "<no-url>")
        urls[url].append((verb, act_name, act_url))

    urls = sorted(urls.items())

    chunks = []
    for url, lines in urls:
        total_reactions = len(lines)
        line_fmt = "    {} from {} - {}"
        lines = "\n".join([line_fmt.format(*l) for l in lines])
        chunks.append("{} ({} reactions)\n{}\n".format(url, total_reactions, lines))

    return "\n".join(chunks)


def prepare_body(notifications):
    logging.info("Preparing notifications")
    date = datetime.today().strftime("%Y-%b-%D")
    all_notifications = list(notifications.all_notifications())
    logging.info("Found {} notifications".format(len(all_notifications)))
    if not all_notifications:
        logging.info("Nothing to send")
        raise ValueError("Nothing to send")
    body = process_notifications(all_notifications)
    ctx = {
        "notification_total": len(all_notifications),
        "date": date,
    }
    return {
        "body": body,
        "subject": SUBJECT_TPL.format(**ctx)
    }


def send_notifications(email_settings=None):
    email_settings = email_settings or settings["EMAIL_SETTINGS"]
    notifications = Notifications(settings["DB"])

    try:
        email_body = prepare_body(notifications)
    except ValueError:
        return  # No messages to send

    msg = MIMEText(email_body["body"])
    msg["Subject"] = email_body["subject"]
    msg["From"] = email_settings["FROM"]
    msg["To"] = email_settings["TO"]

    logging.info("Connecting to SMTP server")
    smtp = SMTP_SSL(email_settings["HOST"], email_settings["PORT"])
    smtp.login(email_settings["FROM"], email_settings["PASS"])
    logging.info("Sending mail")
    smtp.sendmail(email_settings["FROM"], [email_settings["TO"]], msg.as_bytes())
    smtp.quit()
    logging.info("Disconnected SMTP")
    notifications.clear()
    logging.info("All notifications cleared")
