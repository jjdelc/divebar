# coding: utf-8

from wsgiref.simple_server import make_server

from divebar.pelecanus import init_settings


init_settings()

from divebar.api.main import get_app
from divebar.api.send_email import send_notifications as d_sn

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 8080

app = get_app()


def run_server():
    server = make_server(SERVER_HOST, SERVER_PORT, app)
    server.serve_forever()


def send_notifications():
    d_sn()


if __name__ == '__main__':
    run_server()
