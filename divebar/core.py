# coding: utf-8
import logging
import os
import shutil
from datetime import datetime
from genericpath import exists
from os.path import join, abspath, basename, relpath
from urllib.parse import urlparse, urljoin, quote

from pelican.readers import METADATA_PROCESSORS
from slugify import slugify

from divebar.pelecanus import settings
from divebar.pelecanus.generators import BarTender


UNIQUE_ATTRS = {
    'name', 'slug', 'date', 'status', 'display_format', 'title',
    'link', 'mentions', 'content'
}


def generate_new_post_file(title, slug=None, content=None, extra_meta=None, post_date=None):
    post_date = post_date or datetime.now()
    slug = slug or slugify(title)
    tmp_path = settings.get('TMP_POST_PATH', os.getcwd())
    filename = join(tmp_path, '{}.md'.format(slug))
    meta = [
        ('title', title),
        ('date', post_date.strftime('%Y/%m/%d %H:%M:%S')),
        ('slug', slug),
        ('status', 'published')
    ]
    meta.extend(extra_meta or {})
    if content:
        meta.append(('content', content))
    return write_post_file(filename, meta)


def write_post_file(filename, data):
    attrs = [(a, v) for a, v in data if a != 'content']
    content = [v for a, v in data if a == 'content']
    content = content[0] if content else None
    with open(filename, 'w') as fh:
        fh.write('\n'.join(['{}: {}'.format(*attr) for attr in attrs]))
        fh.write('\n\n\n')
        if content is not None:
            fh.write(content)
    return filename


def read_post_meta(filename, include_content=False):
    meta, content_lines = [], []
    reading_content = False
    with open(filename) as fh:
        for line in fh.readlines():
            if ':' in line and not reading_content:
                name, value = line.split(':', 1)
                value = value.strip()
                if name in METADATA_PROCESSORS:
                    meta.append(
                        (name, METADATA_PROCESSORS[name](value, settings))
                    )
                else:
                    meta.append((name, value))
            elif reading_content:
                content_lines.append(line)
            elif line.strip() == '':  # End of post attributes
                if not include_content:
                    break
                else:
                    reading_content = True

    if include_content:
        meta.append(('content', ''.join(content_lines).strip()))

    return meta


def post_file(filename):
    """
    Given an arbitrary file, will put a copy of it on the CONTENT_DIR for
    source files.
    """
    post_meta = dict(read_post_meta(filename))
    publish_date = post_meta['date']
    year = str(publish_date.year)
    month = '{:02d}'.format(publish_date.month)
    target_dir = join(settings['PATH'], year, month)
    if not exists(target_dir):
        os.makedirs(target_dir)
    final_path = join(target_dir, basename(filename))
    shutil.move(filename, final_path)
    return final_path


def index_to_md_file(index_filename):
    """
    :param md_file: File path to an .md content file
    :return: File path to the index.html for the input file
    """
    url = index_filename.replace('.index.html', '')
    raise source_file_for_url(url)


def md_to_index_file(md_filename):
    """

    :param md_filename: File path to index.html file for a post
    :return: File path of its associated .md file
    """
    post_target_file = abspath(md_filename).replace('.md', '/index.html')
    post_target_file = join(settings['OUTPUT_PATH'],
                            relpath(post_target_file, settings['PATH']))
    return post_target_file


def process_file(filename):
    # HACK: We should obtain the destination filename from pelican tools
    # not by string-mangling the filename
    post_meta = dict(read_post_meta(filename))
    publish_date = post_meta['date']
    year = str(publish_date.year)
    month = '{:02d}'.format(publish_date.month)
    post_target_file = md_to_index_file(filename)
    settings['WRITE_SELECTED'] = [
        abspath(join(settings['OUTPUT_PATH'], 'index.html')),
        abspath(join(settings['OUTPUT_PATH'], 'latest_iframe.html')),
        abspath(join(settings['OUTPUT_PATH'], 'archives/index.html')),
        abspath(join(settings['OUTPUT_PATH'], '{}/index.html'.format(year))),
        abspath(join(settings['OUTPUT_PATH'], '{}/{}/index.html'.format(year, month))),
        abspath(join(settings['OUTPUT_PATH'], '{}/roll/index.html'.format(year))),
        abspath(join(settings['OUTPUT_PATH'], '{}/{}/roll/index.html'.format(year, month))),
        post_target_file
    ]
    settings['FILES_TO_PROCESS'] = [filename]
    bar_tender = BarTender(settings)
    bar_tender.run()

    url = build_url(publish_date, post_meta['slug'])
    return url


def build_url(date, slug):
    url = settings['ARTICLE_URL'].format(date=date, slug=slug)
    return url


def make_title(title, post_date):
    if title:
        title = title.strip()
        if title:
            return title, quote(slugify(title, only_ascii=True)), True
    title = post_date.strftime('%b %d %Y - %r')
    slug = slugify(post_date.strftime('%d %H:%M:%S'))
    return title, slug, False


def new_inline_post(content):
    post_date = datetime.now()
    title, slug, has_title = make_title(None, post_date)
    source_file = generate_new_post_file(title, slug, content, [
        ('titleless', True)
    ])
    final = post_file(source_file)
    return process_file(final)


def html_index_for_url(url):
    path = urlparse(url).path

    # This is because pelicanconf says ARTICLE_SAVE_AS at /index.html
    # We obtain the output generated index.html file of this post
    url_file = join(abspath(settings['OUTPUT_PATH']), path.lstrip('/'), 'index.html')
    return url_file


def source_file_for_url(url):
    """
    Given a post URL return its source .md file
    """
    source_file, content_file = None, None
    url_file = html_index_for_url(url)
    # Parse the html file looking for the source.text url
    with open(url_file) as fh:
        for line in fh.readlines():
            if '<link' in line and 'source.text' in line:
                source_file = line.split('href="')[1].split('"')[0]
                break

    if source_file is None:
        logging.error("No source found for editing")
        raise FileNotFoundError("No file for post {}".format(url))

    # Find the filesystem location of that source.text url
    source_file = urljoin(url, source_file)    # URL of the index.text file
    source_file = urlparse(source_file).path   # Path only, no host
    source_file = join(abspath(settings['OUTPUT_PATH']), source_file.lstrip('/'))
    meta = dict(read_post_meta(source_file))  # Dict it bc date is unique
    publish_date = meta['date']
    year = str(publish_date.year)
    month = '{:02d}'.format(publish_date.month)
    target_dir = join(settings['PATH'], year, month)
    # Compare the slug of this source.text with all on the same target dir
    # We could make this faster if we knew for a fact that all the .md files
    # always have the same filename as their slug. Which should be True
    for _source in os.listdir(target_dir):
        _source_path = join(target_dir, _source)
        _meta = dict(read_post_meta(_source_path))
        if meta['slug'] == _meta['slug']:
            # Found a match! Copy that file locally for editing
            return _source_path
    logging.error("No source found for editing")


def update_content(source_file, attrs):
    _post = []
    post = read_post_meta(source_file, include_content=True)
    attrs_attrs = dict(attrs).keys()
    for attr, val in post:
        if attr in attrs_attrs and attr in UNIQUE_ATTRS:
            continue
        _post.append((attr, val))
    _post.extend(attrs)
    return write_post_file(source_file, _post)


def copy_source_file(_source_path):
    shutil.copy(_source_path, os.getcwd())
