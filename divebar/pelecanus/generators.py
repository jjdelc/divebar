from datetime import date, timedelta
from functools import partial
from itertools import chain
from os import listdir
from os.path import join, abspath, isdir
from time import time

from pelican import Pelican
from pelican.generators import (ArticlesGenerator as PelicanArticlesGenerator,
                                PagesGenerator, StaticGenerator,
                                SourceFileGenerator as PelicanSourceGenerator,
                                TemplatePagesGenerator, logger as gen_logger)
from pelican.utils import is_selected_for_writing


class profiler:
    def __init__(self, msg):
        self.msg = msg

    def __enter__(self):
        self.start = time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(self.msg, time() - self.start)


def homepage_files(files, total):
    """
    :param files: List of all .md files
    :return: sublist of those .md files that should be rendered in the index
    """
    all_files = list(reversed(sorted(files)))
    period_date = date.today()
    index_page_files = []
    attempt, max_cycles = 0, len(files) / total
    while len(index_page_files) <= total and attempt < max_cycles:
        # Cycle through months and fill up the blog index page
        prefix = period_date.strftime('%Y/%m/')
        rest = total - len(index_page_files)
        index_page_files.extend([f for f in all_files[:rest] if f.startswith(prefix)])
        period_date = date(period_date.year, period_date.month, 1) - timedelta(days=1)
        attempt += 1
    return index_page_files


def is_gram(article):
    syndications = getattr(article, "syndication", [])
    syndications = syndications if isinstance(syndications, list) else [syndications]
    return any("instagram.com" in s.lower() for s in syndications)


class FasterGenerator:
    def single_article_modified(self):
        return bool(self.settings['WRITE_SELECTED'])

    def limit_function(self, all_files):
        raise NotImplementedError("Subclasses must implement limit_function")

    def get_files(self, paths, exclude=None, extensions=None):
        exclude = exclude or []
        all_files = super().get_files(paths, exclude, extensions)
        if not self.single_article_modified():
            # This means nothing selected, regenerate all
            return all_files

        # This means we selected a file for re-generation
        # Ensure we return the most recent (all the current period)

        # This is the index.html file of the file we chose to edit
        period_files = self.limit_function(all_files)
        edited_files = {f.lstrip(self.settings['PATH'])
                        for f in self.settings['FILES_TO_PROCESS']}
        non_index_edited = edited_files.difference(period_files)
        rebuild_index = len(edited_files.intersection(period_files)) > 0
        setattr(self, 'skip_index', not rebuild_index)
        return period_files + list(non_index_edited)


class ArchiveIndexGenerator(PelicanArticlesGenerator):
    def generate_context(self):
        result = {}
        for path in self.settings['ARTICLE_PATHS']:
            content_path = join(self.path, path)
            for year_dir in listdir(content_path):
                year = int(year_dir)
                year_dir = join(content_path, year_dir)
                result[year] = {}
                if isdir(year_dir):
                    for month_dir in listdir(year_dir):
                        month = month_dir
                        month_dir = join(year_dir, month_dir)
                        if isdir(month_dir):
                            result[year][month] = len([x for x in listdir(month_dir) if x.endswith('.md')])
                result[year] = list(reversed(sorted(result[year].items(), key=lambda t: t[0])))
        result = list(reversed(sorted(result.items(), key=lambda t: t[0])))
        self.context['years'] = result

    def generate_archive_index(self, write):
        template = self.get_template('archives')
        write('archives/index.html', template, self.context)

    def generate_output(self, writer):
        relative_urls = self.settings['RELATIVE_URLS']
        write = partial(writer.write_file, relative_urls=relative_urls)
        self.generate_archive_index(write)


class ArchivesGenerator(FasterGenerator, PelicanArticlesGenerator):
    """
    Implements extra methods to generate the year/month archives
    in a different format.
    """

    def limit_function(self, all_files):
        all_files = list(reversed(sorted(all_files)))
        prefix = all_files[0][:8]
        index_page_files = [art for art in all_files if art.startswith(prefix)]
        return index_page_files

    def generate_context(self):
        super().generate_context()
        self._generate_context()

    def _generate_context(self):
        years, total_gram = {}, {}
        for article in self.context['dates']:
            art_year = article.date.year
            int_month = article.date.month
            art_month = '{:02d}'.format(int_month)
            if art_year not in years:
                years[art_year] = {}
            year_months = years[art_year]

            if art_month not in year_months:
                year_months[art_month] = 0
                total_gram[(art_year, int_month)] = 0

            year_months[art_month] += 1
            total_gram[(art_year, int_month)] += is_gram(article)

        for year, months in years.items():
            years[year] = list(reversed(sorted(months.items())))

        years = list(reversed(sorted(years.items())))  # Sorted by year desc
        self.context['years'] = years
        self.context['total_gram'] = total_gram
        self.context["twitter_user"] = self.settings["TWITTER_USER"]

    def generate_period_archives(self, write):
        template = self.get_template('period_archives')
        years, months = {}, {}

        for article in self.dates:  # dates is sorted decreasing by date
            year = article.date.year
            month = article.date.month
            years.setdefault(year, []).append(article)
            months.setdefault((year, month), []).append(article)

        archive_ctx = self.context.copy()
        for year, articles in years.items():
            archive_ctx['year'] = year
            archive_ctx['period_name'] = year
            archive_ctx['articles'] = articles
            output_file = '{}/index.html'.format(year)
            write(output_file, template, archive_ctx)

        for (year, month), articles in months.items():
            archive_ctx['year'] = year
            archive_ctx['month'] = month
            archive_ctx['period_name'] = date(year, month, 1).strftime('%b %Y')
            archive_ctx['articles'] = articles
            output_file = '{}/{:02d}/index.html'.format(year, month)
            write(output_file, template, archive_ctx)

    def generate_output(self, writer):
        relative_urls = self.settings['RELATIVE_URLS']
        write = partial(writer.write_file, relative_urls=relative_urls)
        self.generate_period_archives(write)


class ArticlesGenerator(FasterGenerator, PelicanArticlesGenerator):
    """
    Same as the original but I don't want to generate the archives with this
    generator. We'll use ArchivesGenerator for that.

    Also, no authors. No tags, no categories.
    """
    def limit_function(self, all_files):
        return homepage_files(all_files, self.settings['DEFAULT_PAGINATION'])

    def generate_index(self, write):
        """
        Unlike .generate_direct_templates, we only want to generate the blog's
        index page. We don't want neither of the options that Pelican provides
        1. Paginate all posts in index##.html files
        2. Don't paginate and include all in a single file

        We want a third option, only include the recent X post on the index
        and link to archive navigation from then on. This reduces the number
        of pages having to be rewritten per new post to only 1 for the index.
        """
        index_template = self.get_template('index')
        recent_articles = self.dates[:self.settings['DEFAULT_PAGINATION']]
        last_date = recent_articles[-1].date
        ctx = self.context.copy()
        ctx['recent_articles'] = recent_articles
        ctx['last_archive'] = {
            'period_name': last_date.strftime('%b %Y'),
            'month': '{:02d}'.format(last_date.month),
            'year': last_date.year
        }
        write('index.html', index_template, ctx)

    def generate_pages(self, writer):
        """Generate the pages on the disk"""
        write = partial(writer.write_file,
                        relative_urls=self.settings['RELATIVE_URLS'])

        self.generate_articles(write)
        if not getattr(self, 'skip_index', False):
            self.generate_index(write)


class DraftsGenerator(PelicanArticlesGenerator):
    """
    I just want to separate the drafts generation in a separate generator
    """
    pass


class SourceFileGenerator(FasterGenerator, PelicanSourceGenerator):
    def limit_function(self, all_files):
        return homepage_files(all_files, self.settings['DEFAULT_PAGINATION'])

    def generate_output(self, writer=None):
        gen_logger.info('Generating source files...')
        to_process = chain(self.context['articles'], self.context.get('pages', []))
        for obj in to_process:
            target = abspath(join(self.settings['OUTPUT_PATH'], obj.save_as))
            if is_selected_for_writing(self.settings, target):
                self._create_source(obj)
                for obj_trans in obj.translations:
                    self._create_source(obj_trans)


class GramGenerator(ArchivesGenerator):
    def generate_period_archives(self, write):
        template = self.get_template('roll')
        archive_ctx = self.context.copy()
        years, months = {}, {}
        for article in self.dates:  # dates is sorted decreasing by date
            if is_gram(article):
                year = article.date.year
                month = article.date.month
                years.setdefault(year, []).append(article)
                months.setdefault((year, month), []).append(article)

        for year, articles in years.items():
            archive_ctx['year'] = year
            archive_ctx['period_name'] = year
            archive_ctx['articles'] = articles
            output_file = '{}/roll/index.html'.format(year)
            write(output_file, template, archive_ctx)

        for (year, month), articles in months.items():
            archive_ctx['year'] = year
            archive_ctx['month'] = month
            archive_ctx['period_name'] = date(year, month, 1).strftime('%b %Y')
            archive_ctx['articles'] = articles
            output_file = '{}/{:02d}/roll/index.html'.format(year, month)
            write(output_file, template, archive_ctx)


class RecentsGenerator(FasterGenerator, PelicanArticlesGenerator):
    RECENT_LIMIT = 6

    def limit_function(self, all_files):
        return homepage_files(all_files, self.RECENT_LIMIT)

    def generate_output(self, writer):
        write = partial(writer.write_file, relative_urls=False)
        template = self.get_template('latest_iframe')
        recent_articles = self.dates[:self.RECENT_LIMIT]
        ctx = self.context.copy()
        ctx['recent_articles'] = recent_articles
        write('latest_iframe.html', template, ctx)


# Order is relevant.
GENERATORS_LIST = [
    ArticlesGenerator,
    RecentsGenerator,
    PagesGenerator,
    TemplatePagesGenerator,
    SourceFileGenerator,
    GramGenerator,
    ArchivesGenerator,
    ArchiveIndexGenerator,  # Must be after period generators
    StaticGenerator,
]


class BarTender(Pelican):
    """
    Subclassing Pelican's main class to overwrite this method because we want
    to fully overwrite the list of generators instead of just adding a few
    by signals.
    """
    def _get_generator_classes(self):
        return GENERATORS_LIST

    def get_generator_classes(self):
        """
        Backwards compat with older Pelican versions
        """
        return self._get_generator_classes()