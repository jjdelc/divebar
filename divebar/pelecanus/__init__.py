import json
import os
from os.path import basename
from urllib.parse import urlparse

from pelican import DEFAULT_CONFIG_NAME
from pelican.settings import read_settings

from divebar.renderers import year_cal, month_archive


def social_icon(post_url):
    if 'twitter.com' in post_url:
        class_name = 'twitter-square'
    elif 'instagram.com' in post_url:
        class_name = 'instagram'
    else:
        return urlparse(post_url).hostname
    return """<i class="fab fa-{}"></i>""".format(class_name)


def iterate_attr(attr, limit=None):
    attrs = attr if isinstance(attr, list) else [attr]
    if limit:
        attrs = attrs[:limit]
    return attrs


def count_if(mentions, attr):
    mentions = json.loads(mentions)
    return len([wm for wm in mentions if wm.get(attr)])


def attr_len(attr):
    return len(iterate_attr(attr))


def as_json(json_text):
    try:
        return json.loads(json_text)
    except:
        return []


def linebreaksbr(flat_text):
    return flat_text.replace("\n", "<br/>")


settings = {}
pelican_settings = settings


# The following are hard choices regarding the blog's structure. They are
# harcoded because some of the code regarding reading and generating posts
# of divebar depends on the permalink structure of articles.
HARD_SETTINGS = {
    'ARTICLE_URL': '{date.year}/{date.month:02d}/{slug}/',
    'ARTICLE_SAVE_AS': '{date.year}/{date.month:02d}/{slug}/index.html',
    'SLUGIFY_SOURCE': 'post_name',
    'OUTPUT_SOURCES': True,
    'THEME': 'theme',
    'DEFAULT_PAGINATION': 20,
    'RELATIVE_URLS': True,
    'JINJA_FILTERS': {
        'md_source': lambda s: basename(s).replace('.md', '/index.text'),
        'social_icon': social_icon,
        'iterate_attr': iterate_attr,
        'attr_len': attr_len,
        'as_json': as_json,
        'count_if': count_if,
        "year_cal": year_cal,
        "month_archive": month_archive,
        "linebreaksbr": linebreaksbr,
    },
    'DIRECT_TEMPLATES': ['index'],
    'PAGINATED_DIRECT_TEMPLATES': ['index'],
    'YEAR_ARCHIVE_SAVE_AS': '{date:%Y}/',
    'MONTH_ARCHIVE_SAVE_AS': '{date:%Y}/{date:%m}/',
    'MARKDOWN': {
        'extension_configs': {
            'markdown.extensions.codehilite': {'css_class': 'highlight'},
            'markdown.extensions.extra': {},
            'markdown.extensions.meta': {},
            'markdown.extensions.attr_list': {},
        },
        'output_format': 'html5',
    },
}


def init_settings():
    global settings
    settings_file = os.environ.get('SETTINGS', DEFAULT_CONFIG_NAME)
    settings.update(read_settings(settings_file))
    settings.update(HARD_SETTINGS)

