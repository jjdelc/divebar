# coding: utf-8

import re

from pelican import signals

from .generators import ArticlesGenerator


HASHTAG_RE = re.compile('#\w+')
HASHTAG_DEST = "https://twitter.com/search?src=typed_query&amp;f=live&amp;q=%%40%s%%20%%23%s"
HASHTAG_MARKUP = """<a href="%(url)s" rel="%(tag)s" class="p-category">%(hashtag)s</a>"""

MENTION_RE = re.compile('@\w+')
MENTION_DEST = "https://twitter.com/%s"
MENTION_MARKUP = """<a href="%(url)s" class="h-card">%(mention)s</a>"""


def extract_hashtags(content):
    return HASHTAG_RE.findall(content)


def markup_references(html_content, settings):
    # This is html content, it doesn't have markdown anymore
    final = []
    for line in html_content.splitlines():
        new_line = []
        for word in line.split(" "):
            hts = HASHTAG_RE.findall(word)
            for ht in hts:
                tag = ht.strip("#")
                url = HASHTAG_DEST % (settings["TWITTER_USER"], tag)
                markup = HASHTAG_MARKUP % {
                    "url": url,
                    "tag": tag,
                    "hashtag": ht
                }
                word = word.replace(ht, markup)

            mentions = MENTION_RE.findall(word)
            for m in mentions:
                url = MENTION_DEST % m.strip("@")
                markup = MENTION_MARKUP % {
                    'url': url,
                    "mention": m
                }
                word = word.replace(m, markup)
            new_line.append(word)
        final.append(" ".join(new_line))
    return "\n".join(final)


def hashtags(generators):
    for generator in generators:
        if isinstance(generator, ArticlesGenerator):
            for article in generator.articles:
                article._content = markup_references(article.content, generator.settings)


def register():
    signals.all_generators_finalized.connect(hashtags)
