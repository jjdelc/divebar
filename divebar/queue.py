import sys
from redis import Redis
from rq import Connection, Worker

from divebar.pelecanus import init_settings, settings

init_settings()

redis_conn = Redis(**settings['RQ_REDIS'])


def run_worker():
    with Connection(redis_conn):
        qs = sys.argv[1:] or ['default']
        w = Worker(qs)
        w.work()


if __name__ == '__main__':
    run_worker()