import calendar


def year_cal(months):
    """
    Used to render the calendar in main archives view
    :param months: list of (month number, total posts)
    :return: array to iterate over a a 12 month grid
    """
    months = {int(m): (m, c) for m, c in months}
    months = [months.get(x, ("{:02d}".format(x), 0)) for x in range(1, 13)]
    monther = lambda n: {
        "name": calendar.month_name[int(n[0])][:3],
        "number": int(n[0]),
        "pos": n[0],
        "count": n[1],
        "active": n[1] != 0
    }
    months = list(reversed(list(map(monther, months))))
    rows = [
        months[:4],
        months[4:8],
        months[8:],
    ]
    rows = [r for r in rows if sum(m["count"] for m in r)]
    return rows


def month_archive(articles):
    dates = {art.date.date(): [] for art in articles}
    for art in articles:
        dates[art.date.date()].append(art)
    result = []
    for date in reversed(sorted(dates)):
        result.append((date, dates[date]))
    return result
