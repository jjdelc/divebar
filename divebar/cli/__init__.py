import logging
from urllib.parse import urljoin
from argparse import ArgumentParser

from divebar.core import generate_new_post_file, post_file, \
    process_file, new_inline_post, source_file_for_url, copy_source_file

from divebar import pelecanus
from divebar.pelecanus.generators import BarTender
from divebar.pelecanus import init_settings, settings


init_settings()

logging.basicConfig(level=logging.INFO)
parser = ArgumentParser()
subparsers = parser.add_subparsers(dest='subparser')

new_post = subparsers.add_parser('new')
edit_post = subparsers.add_parser('edit')
commit_post = subparsers.add_parser('post')
process_post = subparsers.add_parser('process')
rebuild = subparsers.add_parser('rebuild')
inline_post = subparsers.add_parser('inline')


def _generate_new_post_file(args):
    filename = generate_new_post_file(args.title)
    logging.info('Post file created: {}'.format(filename))


def _post_file(args):
    dest = post_file(args.filename)
    url = process_file(dest)
    url = urljoin(settings['SITEURL'], url)
    logging.info("File posted at: {}".format(dest))
    logging.info("Post URL: {}".format(url))


def _process_file(args):
    process_file(args.filename)
    logging.info("Post processed")


def _rebuild_all(args):
    bar_tender = BarTender(pelecanus.pelican_settings)
    bar_tender.run()


def _inline_post(args):
    content = args.content
    url = new_inline_post(content)
    url = urljoin(settings['SITEURL'], url)
    logging.info("Post URL: {}".format(url))


def _edit_post(args):
    url = args.url
    source_file = source_file_for_url(url)
    copy_source_file(source_file)


new_post.add_argument('title')
new_post.set_defaults(func=_generate_new_post_file)
commit_post.add_argument('filename')
commit_post.set_defaults(func=_post_file)
process_post.add_argument('filename')
process_post.set_defaults(func=_process_file)
inline_post.add_argument('content')
inline_post.set_defaults(func=_inline_post)
rebuild.set_defaults(func=_rebuild_all)
edit_post.add_argument('url')
edit_post.set_defaults(func=_edit_post)


def main():
    args = parser.parse_args()
    if not args.subparser:
        parser.print_help()
        exit()
    args.func(args)


if __name__ == '__main__':
    main()
