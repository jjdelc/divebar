# coding: utf-8
import os
import logging
import sys

import socketserver

from pelican import DEFAULT_CONFIG_NAME
from pelican.server import ComplexHTTPRequestHandler as CHRH
from pelican.settings import read_settings

CONFIG = os.environ.get("DEFAULT_CONFIG_NAME", DEFAULT_CONFIG_NAME)
settings = read_settings(CONFIG)
OUTPUT_PATH = settings['OUTPUT_PATH']
os.chdir(OUTPUT_PATH)


class ComplexHTTPRequestHandler(CHRH):
    def guess_type(self, path):
        """
        Additional seasoning to serve plain files as HTML because blog URLs
        won't have a .html extension.
        """
        mimetype = super().guess_type(path)
        if path.endswith('.md'):
            return 'text/plain'

        if mimetype == 'application/octet-stream':
            return 'text/html'
        return mimetype


# Same as pelican's server, but use our handler for HTML files
if __name__ == '__main__':
    PORT = len(sys.argv) in (2, 3) and int(sys.argv[1]) or 8000
    SERVER = len(sys.argv) == 3 and sys.argv[2] or ""

    socketserver.TCPServer.allow_reuse_address = True
    try:
        httpd = socketserver.TCPServer(
            (SERVER, PORT), ComplexHTTPRequestHandler)
    except OSError as e:
        logging.error("Could not listen on port %s, server %s.", PORT, SERVER)
        sys.exit(getattr(e, 'exitcode', 1))

    logging.info("Serving at port %s, server %s.", PORT, SERVER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt as e:
        logging.info("Shutting down server.")
        httpd.socket.close()
