import json
import os
import sqlite3
from time import time


try:
    from _thread import get_ident
except ImportError:
    from _dummy_thread import get_ident


def dict_factory_factory(json_fields):
    # https://stackoverflow.com/a/3300514/43490
    def dict_factory(cursor, row):
        """
        Returns each Sqlite row as  a dictionary.

        Use:

            conn = sqlite3.Connection(path)
            conn.row_factory = dict_factory
        """
        d = {}
        for idx, col in enumerate(cursor.description):
            fname = col[0]
            val = row[idx]
            val = json.loads(val) if fname in json_fields else val
            d[fname] = val
        return d

    return dict_factory


class BaseDB(object):
    _create = ""
    json_fields = set()

    def __init__(self, path):
        self.path = os.path.abspath(path)
        self._connection_cache = {}
        with self._get_conn() as conn:
            for table in self._create:
                conn.execute(table)

    def _get_conn(self):
        _id = get_ident()
        if _id not in self._connection_cache:
            conn = sqlite3.Connection(self.path, timeout=60)
            conn.row_factory = dict_factory_factory(self.json_fields)
            self._connection_cache[_id] = conn
        return self._connection_cache[_id]


class Notifications(BaseDB):
    json_fields = {"payload"}
    _create = [
            'CREATE TABLE IF NOT EXISTS notifications '
            '('
            '  id INTEGER PRIMARY KEY AUTOINCREMENT,'
            '  verb TEXT,'
            '  url TEXT,'
            '  source TEXT,'
            '  timestamp INTEGER,'
            '  pending INTEGER,'
            '  payload TEXT'
            ');'
            ]
    _add_notification = "INSERT INTO notifications (%(fields)s) VALUES (%(values)s)"
    _delete_all = "DELETE FROM notifications;"
    _read_all = "SELECT * FROM notifications;"
    _total_notifications = "SELECT COUNT(*) count FROM notifications;"

    def add_notification(self, verb, url, payload):
        with self._get_conn() as conn:
            data = {
                "timestamp": time(),
                "pending": True,
                "url": url,
                "verb": verb,
                "payload": json.dumps(payload)
            }
            fields = data.keys()
            query = self._add_notification % {
                'fields': ', '.join(fields),
                'values': ', '.join([':%s' % k for k in fields]),
            }
            conn.execute(query, data)

    def all_notifications(self):
        with self._get_conn() as conn:
            return conn.execute(self._read_all)

    def clear(self):
        with self._get_conn() as conn:
            query = self._delete_all
            return conn.execute(query)

    def count(self):
        with self._get_conn() as conn:
            query = self._total_notifications
            return conn.execute(query).fetchone()['count']


class PicturesDB(BaseDB):
    RECENT_LIMIT = 12

    _create = [
        'CREATE TABLE IF NOT EXISTS recent_uploads '
        '('
        '  id INTEGER PRIMARY KEY AUTOINCREMENT,'
        '  url TEXT,'
        '  timestamp INTEGER'
        ');'
    ]
    _add_picture = "INSERT INTO recent_uploads (%(fields)s) VALUES (%(values)s)"
    _recent = "SELECT * FROM recent_uploads ORDER BY timestamp DESC LIMIT %(limit)s;"

    def add_picture(self, url):
        with self._get_conn() as conn:
            data = {
                "timestamp": time(),
                "url": url,
            }
            fields = data.keys()
            query = self._add_picture % {
                'fields': ', '.join(fields),
                'values': ', '.join([':%s' % k for k in fields]),
            }
            conn.execute(query, data)

    def recent(self, limit=None):
        limit = limit or self.RECENT_LIMIT
        with self._get_conn() as conn:
            return list(conn.execute(self._recent % {"limit": limit}))
