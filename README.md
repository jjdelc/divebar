# Indie web personal stream - AKA Blog

Divebar is an [indieweb](https://indieweb.org/) blog.

It works in two parts:

 * Static content, generated using [Pelican](http://www.getpelican.com/)
 * Python server to support Indieweb 
 
 
 
 ## New post
 
    $ divebar new "title"
    $ vim generated-file.md
    $ divebar post generated-file.md